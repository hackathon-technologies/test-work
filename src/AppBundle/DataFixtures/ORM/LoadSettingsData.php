<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Setting;

class LoadSettingsData implements FixtureInterface
{
    private function getData()
    {
        $now = new \DateTime();
        $limitsByMonths = range(5000, 5011);

        return [
            [
                'key' => 'maximum_amount_of_expenses.value',
                'desc' => 'Предельная сумма расходов для текущего месяца.',
                'setting' => [
                    'type' => 'input',
                    'currentValue' => $limitsByMonths[(int)$now->format('m')],
                    'currentIndexValue' => $now->format('m'),
                    'monthlyValues' => $limitsByMonths,
                ],
            ],
            [
                'key' => 'maximum_amount_of_expenses.scenarios',
                'desc' => 'Сценарий поведения приложение при превышении суммы месячных расходов. </br> <strong>Адаптивный предел (adaptive_limit)</strong> - предельная сумма следующего месяца уменьшается </br> на величину превышения в текущем месяце. </br> <strong>Увеличение предела (limit_increase)</strong> - после превышения предела приложение предлагает </br> пользователю увеличить предельную сумму текущего месяца.',
                'setting' => [
                    'type' => 'select',
                    'currentValue' => 'adaptive_limit',
                    'options' => [
                        'adaptive_limit' => 'adaptive_limit',
                        'limit_increase' => 'limit_increase',
                    ],
                ],
            ],
        ];
    }

    public function load(ObjectManager $manager)
    {
        $this->down($manager);
        $this->up($manager);
    }

    private function up(ObjectManager $manager)
    {
        foreach($this->getData() as $item) {
            $setting = new Setting();
            foreach($item as $key => $value) {
                $setting->{'set'.ucfirst($key)}($value);
            }
            $manager->persist($setting);
        }
        $manager->flush();
    }

    private function down(ObjectManager $manager)
    {
        foreach($this->getData() as $item) {
            $find = $manager->getRepository('AppBundle\Entity\Setting')->findOneByKey($item['key']);
            if($find) {
                $manager->remove($find);
            }
        }
        $manager->flush();
    }
}