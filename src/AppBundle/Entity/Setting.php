<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Setting
 *
 * @ORM\Table(name="setting")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SettingRepository")
 */
class Setting
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="key", type="string", length=100, nullable=true, unique=true)
     */
    private $key;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active = true;

    /**
     * @var string
     *
     * @ORM\Column(name="desc", type="string", length=100, nullable=true, unique=false)
     */
    private $desc;

    /**
     * Setting (serialized data)
     *
     * Required params:
     * {
     *     "type": "input|select|date|array|object ...",
     *     "currentValue": "...",
     *     ... additional attrs ...
     * }
     *
     * @var string
     *
     * @ORM\Column(name="setting", type="text", nullable=true, unique=false)
     */
    private $setting;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set key
     *
     * @param string $key
     *
     * @return Setting
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set desc
     *
     * @param string $desc
     *
     * @return Setting
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * Get desc
     *
     * @return string
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * Set setting
     *
     * @param array $setting
     * @return Setting
     */
    public function setSetting($setting)
    {
        $this->setting = json_encode($setting);

        return $this;
    }

    /**
     * Get setting
     *
     * @return array|string
     */
    public function getSetting($returnArray = false)
    {
        return $returnArray ? json_decode($this->setting, true) : $this->setting;
    }

    /**
     * Set mock-object (Setting)
     *
     * @param $mockSetting
     * @return Setting
     */
    public function setMockSetting($mockSetting)
    {
        return $this;
    }

    /**
     * Get mock-object (Setting)
     *
     * @param bool $returnArray
     * @return array|string
     */
    public function getMockSetting($returnArray = false)
    {
        return $returnArray ? [] : '';
    }

    /**
     * Set active
     *
     * @param $status
     * @return Setting
     */
    public function setActive($status)
    {
        $this->active = $status;

        return $this;
    }

    /**
     * Is active
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    public function getValue()
    {
        return $this->getSetting(true)['currentValue'];
    }
}

