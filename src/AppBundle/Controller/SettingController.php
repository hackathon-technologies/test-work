<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Setting;
use AppBundle\Form\SettingType;

/**
 * Setting controller.
 *
 * @Route("/setting")
 */
class SettingController extends Controller
{
    /**
     * Lists all Setting entities.
     *
     * @Route("/", name="setting_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $settings = $em->getRepository('AppBundle:Setting')->findAll();

        return $this->render('setting/index.html.twig', array(
            'settings' => $settings,
        ));
    }

    /**
     * Displays a form to edit an existing Setting entity.
     *
     * @Route("/{id}/edit", name="setting_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Setting $setting)
    {
        $editForm = $this->createForm('AppBundle\Form\SettingType', $setting);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $data = $setting->getSetting(true);
            $data['currentValue'] = $request->request->get('setting')['mockSetting'];

            $setting->setSetting($data);

            $em->persist($setting);
            $em->flush();

            $this->get('app.scenario')->execute();

            return $this->redirectToRoute('setting_edit', array('id' => $setting->getId()));
        }

        return $this->render('setting/edit.html.twig', array(
            'setting' => $setting,
            'edit_form' => $editForm->createView(),
        ));
    }
}
