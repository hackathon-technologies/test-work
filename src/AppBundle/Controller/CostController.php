<?php

namespace AppBundle\Controller;

use AppBundle\Service\Scenario;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Cost;
use AppBundle\Form\CostType;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Cost controller.
 *
 * @Route("/cost")
 */
class CostController extends Controller
{
    /**
     * Lists all Cost entities.
     *
     * @Route("/", name="cost_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $costs = $em->getRepository('AppBundle:Cost')->findAll();

        return $this->render('cost/index.html.twig', [
            'costs' => $costs,
            'checkTotalCostsIsFail' => $this->checkTotalSumIsFail(),
        ]);
    }

    /**
     * Creates a new Cost entity.
     *
     * @Route("/new", name="cost_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $cost = new Cost();
        $form = $this->createForm('AppBundle\Form\CostType', $cost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->saveCost($cost);

            return $this->redirectToRoute('cost_show', array('id' => $cost->getId()));
        }

        return $this->render('cost/new.html.twig', array(
            'cost' => $cost,
            'form' => $form->createView(),
            'checkTotalCostsIsFail' => $this->checkTotalSumIsFail(),
        ));
    }

    /**
     * Finds and displays a Cost entity.
     *
     * @Route("/{id}", name="cost_show")
     * @Method("GET")
     */
    public function showAction(Cost $cost)
    {
        $deleteForm = $this->createDeleteForm($cost);

        return $this->render('cost/show.html.twig', array(
            'cost' => $cost,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Cost entity.
     *
     * @Route("/{id}/edit", name="cost_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Cost $cost)
    {
        $deleteForm = $this->createDeleteForm($cost);
        $editForm = $this->createForm('AppBundle\Form\CostType', $cost);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->saveCost($cost);

            return $this->redirectToRoute('cost_edit', array('id' => $cost->getId()));
        }

        return $this->render('cost/edit.html.twig', array(
            'cost' => $cost,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'checkTotalCostsIsFail' => $this->checkTotalSumIsFail(),
        ));
    }

    /**
     * Deletes a Cost entity.
     *
     * @Route("/{id}", name="cost_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Cost $cost)
    {
        $form = $this->createDeleteForm($cost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cost);
            $em->flush();
        }

        return $this->redirectToRoute('cost_index');
    }

    /**
     * Creates a form to delete a Cost entity.
     *
     * @param Cost $cost The Cost entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cost $cost)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cost_delete', array('id' => $cost->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Save Cost.
     *
     * @param Cost $cost
     */
    private function saveCost(Cost $cost)
    {
        $em = $this->getDoctrine()->getManager();

        $em->persist($cost);
        $em->flush();

        $this->get('app.scenario')->execute();
    }

    /**
     * Check total sum costs is fail.
     *
     * @return bool
     */
    private function checkTotalSumIsFail()
    {
        $setting = $this->getDoctrine()->getRepository('AppBundle:Setting')->findOneByKey('maximum_amount_of_expenses.scenarios');

        return $this->get('app.scenario')->checkTotalSumByCurrentMonth() < 0
            && $setting->getSetting(true)['currentValue'] == Scenario::SCENARIO_LIMIT_INCREASE;
    }
}
