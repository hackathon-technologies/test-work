<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Report controller.
 *
 * @Route("/report")
 */
class ReportController extends Controller
{
    /**
     * @Route("/consolidated", name="report_consolidated")
     */
    public function renderConsolidatedReportAction()
    {
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('AppBundle:Cost')->groupBy('month', false);

        return $this->render('report/consolidated.html.twig', array(
            'data' => $data,
        ));
    }

    /**
     * @Route("/detailed", name="report_detailed")
     */
    public function renderDetailedReportAction()
    {
        $matrix = [];
        $lastRowMatrix = [];
        $em = $this->getDoctrine()->getManager();

        $costs = $em->getRepository('AppBundle:Cost')->groupBy('day');
        $categories = $em->getRepository('AppBundle:Category')->findAll();

        foreach ($costs as $day => $cost) {
            $row = [$cost[0]['origin_date']];

            foreach ($categories as $category) {
                $sum = $em->getRepository('AppBundle:Cost')
                    ->calculateTotalSumBy('day', (string) $day, $category->getName());
                $row[] = $sum;
            }

            $row[] = $em->getRepository('AppBundle:Cost')
                ->calculateTotalSumBy('day', (string) $day);
            $matrix[] = $row;
        }

        foreach ($categories as $category) {
            $row = [];
            $row[] = $em->getRepository('AppBundle:Cost')
                ->calculateTotalSumBy(null, null, $category->getName());
            $lastRowMatrix[] = $row;
        }

        return $this->render('report/detailed.html.twig', array(
            'matrix' => $matrix,
            'categories' => $categories,
            'lastRowMatrix' => $lastRowMatrix,
        ));
    }
}
