<?php

namespace AppBundle\Form;

use AppBundle\Entity\Setting;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SettingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $setting = $options['data'];

        $builder
            ->add('active', CheckboxType::class, [
                'label' => 'Настройка активна',
                'required' => false,
            ])
        ;

        if($this->settingIsTypeOf($setting, 'input')) {
            $builder
                ->add('mockSetting', TextType::class, [
                    'label' => 'Новое значение',
                    'attr' => [
                        'class' => 'form-control',
                        'value' => $setting->getSetting(true)['currentValue'],
                    ],
                ])
            ;
        }

        if($this->settingIsTypeOf($setting, 'select')) {
            $builder
                ->add('mockSetting', ChoiceType::class, [
                    'label' => 'Новое значение',
                    'choices' => $setting->getSetting(true)['options'],
                    'data' => $setting->getSetting(true)['currentValue'],
                    'attr' => [
                        'class' => 'form-control',
                    ],
                ])
            ;
        }
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Setting'
        ));
    }

    private function settingIsTypeOf(Setting $setting, $type)
    {
        return $setting->getSetting(true)['type'] == $type;
    }
}
