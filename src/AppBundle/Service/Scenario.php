<?php
namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Scenario
{
    const SCENARIO_ADAPTIVE_LIMIT = 'adaptive_limit';
    const SCENARIO_LIMIT_INCREASE = 'limit_increase';

    /** @var EntityManager  */
    private $em;

    /**
     * Scenario constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container) {
        $this->em = $container->get('doctrine.orm.entity_manager');
    }

    /**
     * Execute scenarios.
     */
    public function execute()
    {
        $scenario = $this->em->getRepository('AppBundle:Setting')
            ->findOneByKey('maximum_amount_of_expenses.scenarios');

        if($scenario->getSetting(true)['currentValue'] == self::SCENARIO_ADAPTIVE_LIMIT) {
            $this->adaptiveLimit();
        }
    }

    /**
     * "Adaptive limit" scenario.
     */
    public function adaptiveLimit()
    {
        $limit = $this->em->getRepository('AppBundle:Setting')
            ->findOneByKey('maximum_amount_of_expenses.value');

        $diff = $this->checkTotalSumByCurrentMonth();

        $limitArr = $limit->getSetting(true);
        $nextIndexMonth = $limitArr['currentIndexValue'] + 1;

        if($diff < 0) {
            $limitArr['monthlyValues'][$nextIndexMonth] += $diff;
        } else {
            $limitArr['monthlyValues'][$nextIndexMonth] = $limitArr['currentValue'];
        }

        $limit->setSetting($limitArr);
        $this->em->persist($limit);
        $this->em->flush();
    }

    /**
     * Check total sum by current month.
     *
     * @return mixed
     */
    public function checkTotalSumByCurrentMonth()
    {
        $totalCosts = $this->em->getRepository('AppBundle:Cost')->calculateTotalSumBy();
        $maximumAmountOfExpenses = $this->em->getRepository('AppBundle:Setting')->findOneByKey('maximum_amount_of_expenses.value');

        return $maximumAmountOfExpenses->getValue() - $totalCosts;
    }
}