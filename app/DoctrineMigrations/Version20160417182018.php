<?php

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160417182018 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE setting (id INTEGER NOT NULL, "key" VARCHAR(100) DEFAULT NULL, active BOOLEAN DEFAULT NULL, "desc" VARCHAR(100) DEFAULT NULL, setting CLOB DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9F74B8988A90ABA9 ON setting ("key")');
        $this->addSql('DROP INDEX UNIQ_182694FC12469DE2');
        $this->addSql('CREATE TEMPORARY TABLE __temp__cost AS SELECT id, category_id, sum, date FROM cost');
        $this->addSql('DROP TABLE cost');
        $this->addSql('CREATE TABLE cost (id INTEGER NOT NULL, category_id INTEGER DEFAULT NULL, sum DOUBLE PRECISION NOT NULL, date DATE NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_182694FC12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO cost (id, category_id, sum, date) SELECT id, category_id, sum, date FROM __temp__cost');
        $this->addSql('DROP TABLE __temp__cost');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_182694FC12469DE2 ON cost (category_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE setting');
        $this->addSql('DROP INDEX UNIQ_182694FC12469DE2');
        $this->addSql('CREATE TEMPORARY TABLE __temp__cost AS SELECT id, category_id, sum, date FROM cost');
        $this->addSql('DROP TABLE cost');
        $this->addSql('CREATE TABLE cost (id INTEGER NOT NULL, category_id INTEGER DEFAULT NULL, sum DOUBLE PRECISION NOT NULL, date DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO cost (id, category_id, sum, date) SELECT id, category_id, sum, date FROM __temp__cost');
        $this->addSql('DROP TABLE __temp__cost');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_182694FC12469DE2 ON cost (category_id)');
    }
}
